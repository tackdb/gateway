package gateway

import (
	"os"
)

type Config struct {
	Routes Routes `json:"routes"`
}

func NewGateway() (*ReloadingRouter, error) {
	registryUrl := os.Getenv("REGISTRY_URL")
	router := &ReloadingRouter{}
	if err := router.LoadURL(registryUrl); err != nil {
		return nil, err
	}
	return router, nil
}
