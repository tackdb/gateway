package gateway

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
)

type Reloadable interface {
	Reload(w http.ResponseWriter, r *http.Request)
}

type ReloadingRouter struct {
	router *mux.Router
}

// Routes maps paths to destinations.
type Routes map[string]string

func (rr *ReloadingRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Redirect both http and www subdomain requests to https root.
	if r.Header.Get("X-Forwarded-Proto") == "http" || strings.HasPrefix(r.Host, "www.") {
		host := strings.TrimPrefix(r.Host, "www.")
		http.Redirect(w, r, "https://"+host+r.URL.Path, http.StatusTemporaryRedirect)
		return
	}

	rr.router.ServeHTTP(w, r)
}

func (rr *ReloadingRouter) Reload(w http.ResponseWriter, r *http.Request) {
	pass := os.Getenv("REGISTRY_PASS")
	vars := mux.Vars(r)
	inputpass, _ := vars["pass"]
	if pass != inputpass {
		http.Error(w, "404 page not found", http.StatusNotFound)
		return
	}

	registryUrl := os.Getenv("REGISTRY_URL")

	if err := rr.LoadURL(registryUrl); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		w.Write([]byte("Successfully registered new routes."))
	}
}

func (rr *ReloadingRouter) LoadURL(registryUrl string) error {
	resp, err := http.Get(registryUrl)
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var config Config
	if err := json.Unmarshal(body, &config); err != nil {
		return err
	}

	rr.RegisterRoutes(config.Routes)
	return nil
}

func (rr *ReloadingRouter) RegisterRoutes(routes Routes) {
	router := mux.NewRouter()
	var root string
	var hasRoot bool
	if root, hasRoot = routes["/"]; hasRoot {
		delete(routes, "/")
	}

	router.PathPrefix("/reload/{pass}").HandlerFunc(rr.Reload)
	for path, destination := range routes {
		router.PathPrefix(path).HandlerFunc(MakeHandlerTo(destination))
	}
	if hasRoot {
		router.PathPrefix("/").HandlerFunc(MakeHandlerTo(root))
	}
	rr.router = router
}
