package main

import (
	"errors"
	"log"
	"net/http"
	"os"

	flag "github.com/ogier/pflag"

	"gitlab.com/tackdb/gateway"
)

var ErrNoSecret = errors.New("Missing SESSION_SECRET variable.")

func main() {
	flag.Parse()

	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}

	if os.Getenv("SESSION_SECRET") == "" {
		log.Fatal(ErrNoSecret)
	}

	handler, err := gateway.NewGateway()
	if err != nil {
		log.Fatal(err)
	}
	
	log.Fatal(http.ListenAndServe(":"+port, handler))
}
